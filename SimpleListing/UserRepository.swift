//
//  Repository.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation

class UserRepository {

    static var users = [Int:User]()

    static func createOrGet(userId:Int) -> User {

        if let user = UserRepository.users[userId] {
            return user
        }

        let user = User(id: userId)
        UserRepository.users[userId] = user

        return user
    }

}
