//
//  Comment.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation

class Comment: BaseModel {

    override var slug: String {
        return "comments"
    }

    var id:Int
    var postId:Int
    var name:String
    var email:String
    var body:String

    var post:Post? {
        let repository = PostsRepository()
        return repository.get(index: postId)
    }

    init(id:Int, postId:Int, name:String, email:String, body:String) {
        self.id = id
        self.postId = postId
        self.name = name
        self.email = email
        self.body = body
    }

    convenience init?(json:Any) {

        guard let data = json as? [String:Any] else {
            return nil
        }

        guard let id = data["id"] as? Int else {
            return nil
        }

        guard let postId = data["postId"] as? Int else {
            return nil
        }

        guard let name = data["name"] as? String else {
            return nil
        }

        guard let email = data["email"] as? String else {
            return nil
        }

        guard let body = data["body"] as? String else {
            return nil
        }

        self.init(id: id, postId: postId, name: name, email: email, body: body)
    }

}
