//
//  APIWrapper.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation
import Alamofire

class APIWrapper {

    static let utilityQueue = DispatchQueue.global(qos: .utility)

    static func getItems(request: URLRequest, callback:@escaping (_ status:Bool, _ data:[[String: Any]])->()) {
 
        Alamofire.request(request).responseJSON(queue: APIWrapper.utilityQueue) { response in

            if response.error != nil {
                print("Can not get posts: \(String(describing: response.error))")

                DispatchQueue.main.async {
                    callback(false, [])
                }

                return
            }

            guard let data = response.value as? [[String: Any]] else {
                print("Invalid data structure: \(String(describing: response.value))")

                DispatchQueue.main.async {
                    callback(false, [])
                }

                return
            }

            DispatchQueue.main.async {
                callback(true, data)
            }
        }

    }

}
