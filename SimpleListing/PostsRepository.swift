//
//  PostsRepository.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation

class PostsRepository {

    static var posts = [Post]()

    func fetch(callback:@escaping (Bool)->()) {

        guard let request = Router.listPosts().urlRequest else {
            callback(false)
            return
        }

        APIWrapper.getItems(request: request) { status, data in

            guard status else {
                PostsRepository.posts.removeAll()
                callback(false)
                return
            }

            PostsRepository.posts = data.flatMap({ item in
                Post(json: item)
            }).sorted(by: { $0.title < $1.title })


            callback(true)
        }

    }

    func count() -> Int {
        return PostsRepository.posts.count
    }

    func get(index:Int) -> Post? {
        guard PostsRepository.posts.count > index else {
            return nil
        }

        return  PostsRepository.posts[index]
    }

}
