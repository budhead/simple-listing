//
//  CommentCell.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!


    func setup(comment:Comment) {
        let color = Common.randomColor()
        iconImageView.tintColor = color

        let text = comment.email + " " + comment.body
        let attributedText = NSMutableAttributedString(string: text, attributes: [

        :])

        attributedText.addAttributes([
            NSForegroundColorAttributeName: color
            ], range: NSRange(location: 0, length: comment.email.characters.count))

        commentLabel.attributedText = attributedText
    }

}
