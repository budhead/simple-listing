//
//  Post.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation

class Post: BaseModel {

    override var slug: String {
        return "posts"
    }

    var id:Int
    var userId:Int
    var title:String
    var body:String

    var user:User {
        return UserRepository.createOrGet(userId: userId)
    }

    init(id:Int, userId:Int, title:String, body:String) {
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
    }

    convenience init?(json:Any) {

        guard let data = json as? [String:Any] else {
            return nil
        }

        guard let id = data["id"] as? Int else {
            return nil
        }

        guard let userId = data["userId"] as? Int else {
            return nil
        }

        guard let title = data["title"] as? String else {
            return nil
        }

        guard let body = data["body"] as? String else {
            return nil
        }

        self.init(id: id, userId: userId, title: title, body: body)
    }
}
