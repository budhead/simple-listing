//
//  Router.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    case listPosts()
    case listComments(postId: Int)

    static let baseURLString = Common.plistItem(itemKey: "apiBaseURL") as? String ?? ""

    func asURLRequest() throws -> URLRequest {

        let result: (path: String, parameters: Parameters) = {

            switch self {
            case .listPosts():
                return ("/posts", [:])
            case let .listComments(postId):
                return ("/comments", ["postId": postId])
            }

        }()

        let url = try Router.baseURLString.asURL()
        let urlRequest = URLRequest(url: url.appendingPathComponent(result.path))

        return try URLEncoding.default.encode(urlRequest, with: result.parameters)
    }
}
