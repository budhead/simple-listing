//
//  BaseModel.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation
import Alamofire

class BaseModel {

    var slug:String {
        return ""
    }

}

extension BaseModel: URLConvertible {

    static let baseURLString = Common.plistItem(itemKey: "apiBaseURL") as? String ?? ""

    func asURL() throws -> URL {
        let urlString = BaseModel.baseURLString + "/" + slug

        guard let url = URL(string: urlString) else {
            print("Can not build URL from: \(urlString)")

            return URL(string: "")!
        }


        return url
    }
}
