//
//  User.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class User {

    var id:Int
    var color:UIColor

    init(id:Int, color:UIColor) {
        self.id = id
        self.color = color
    }

    convenience init(id:Int) {
        let color = Common.randomColor()

        self.init(id: id, color: color)
    }

    convenience init?(json:Any) {

        guard let data = json as? [String:Any] else {
            return nil
        }

        guard let id = data["id"] as? Int else {
            return nil
        }

        self.init(id: id)
    }


}
