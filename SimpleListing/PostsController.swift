//
//  ViewController.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class PostsController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!

    let repository = PostsRepository()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 118

        reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath)

        if let cell = cell as? PostCell, let post = repository.get(index: indexPath.row) {
            cell.setup(post: post)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let post = repository.get(index: indexPath.row) {
            self.performSegue(withIdentifier: "GoToComments", sender: post)
        }
    }

    // -- Support functions

    func hideTable() {
        errorLabel.isHidden = true
        tableView.isHidden = true
        indicator.startAnimating()
    }

    func showTable() {
        tableView.alpha = 0
        tableView.isHidden = false
        errorLabel.isHidden = true

        UIView.animate(withDuration: 0.5, animations: {
            self.indicator.alpha = 0
            self.tableView.alpha = 1
        }) { finished in
            self.indicator.stopAnimating()
            self.indicator.alpha = 1
        }
    }

    func showError() {
        indicator.stopAnimating()
        tableView.isHidden = true
        errorLabel.isHidden = false
    }

    @IBAction func reloadData() {

        hideTable()

        repository.fetch { status in

            guard status else {
                self.showError()
                return
            }

            self.tableView.reloadData()
            self.showTable()
        }
    }

    // -- Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CommentsController {
            guard let post = sender as? Post else {
                return
            }

            vc.postId = post.id
        }
    }
}

