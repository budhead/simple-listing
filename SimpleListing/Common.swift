//
//  Common.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class Common {

    static let appColors = [
        UIColor(red:0.16, green:0.58, blue:0.70, alpha:1.0),
        UIColor(red:1.00, green:0.43, blue:0.60, alpha:1.0),
        UIColor(red:0.80, green:0.75, blue:0.18, alpha:1.0),
        UIColor(red:0.33, green:0.85, blue:1.00, alpha:1.0),
        UIColor(red:1.00, green:0.84, blue:0.43, alpha:1.0),
        UIColor(red:0.18, green:0.80, blue:0.57, alpha:1.0),
        UIColor(red:1.00, green:0.34, blue:0.39, alpha:1.0),
        UIColor(red:0.20, green:0.59, blue:0.80, alpha:1.0),
        UIColor(red:0.70, green:0.32, blue:0.35, alpha:1.0),
        UIColor(red:0.70, green:0.60, blue:0.42, alpha:1.0)
    ]

    static func plist(name:String = "Project") -> [String:AnyObject] {
        if let path = Bundle.main.path(forResource: name, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            return dict
        }

        return [:]
    }

    static func plistItem(itemKey:String) -> AnyObject? {
        let dict = Common.plist()
        return dict[itemKey]
    }

    static func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    static func randomColor() -> UIColor {
        let index = random(max: appColors.count)
        return appColors[index]
    }

    static func random(max maxNumber: Int) -> Int {
        return Int(arc4random_uniform(UInt32(maxNumber)))
    }
}
