//
//  CommentsRepository.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import Foundation

class CommentsRepository {

    var comments = [Comment]()

    func fetch(postId:Int, callback:@escaping (Bool)->()) {

        guard let request = Router.listComments(postId: postId).urlRequest else {
            callback(false)
            return
        }

        APIWrapper.getItems(request: request) { status, data in
            
            guard status else {
                self.comments.removeAll()
                callback(false)
                return
            }
            
            self.comments = data.flatMap({ item in
                Comment(json: item)
            })

            callback(true)
        }

    }

    func count() -> Int {
        return comments.count
    }

    func get(index:Int) -> Comment? {
        return comments[index]
    }
    
}
