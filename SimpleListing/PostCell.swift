//
//  PostCell.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    var post:Post?

    func setup(post:Post) {
        self.post = post

        self.titleLabel.text = post.title
        self.bodyLabel.text = post.body
        self.backgroundColor = post.user.color

        self.bodyLabel.sizeToFit()

        if post.user.color.isLight {
            self.bodyLabel.textColor = UIColor.darkGray
        } else {
            self.bodyLabel.textColor = UIColor.lightGray
        }

    }

}
