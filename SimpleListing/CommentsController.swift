//
//  CommentsController.swift
//  SimpleListing
//
//  Created by Lachezar Todorov on 7/3/17.
//  Copyright © 2017 73am. All rights reserved.
//

import UIKit

class CommentsController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!

    var postId = 0

    let repository = CommentsRepository()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80

        reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repository.count()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)

        if let cell = cell as? CommentCell, let comment = repository.get(index: indexPath.row) {
            cell.setup(comment: comment)
        }

        return cell
    }


    // -- Support functions

    func hideTable() {
        tableView.isHidden = true
        indicator.startAnimating()
    }

    func showTable() {
        tableView.alpha = 0
        tableView.isHidden = false

        UIView.animate(withDuration: 0.5, animations: {
            self.indicator.alpha = 0
            self.tableView.alpha = 1
        }) { finished in
            self.indicator.stopAnimating()
            self.indicator.alpha = 1
        }
    }

    func showError() {
        indicator.stopAnimating()
        tableView.isHidden = true

        let alert = UIAlertController(title: "Please try again later.", message: nil, preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: { _ in
            self.reloadData()
        }))

        self.present(alert, animated: true, completion: nil)
    }

    func reloadData() {

        hideTable()

        repository.fetch(postId: postId) { status in

            guard status else {
                self.showError()
                return
            }
            
            self.tableView.reloadData()
            self.showTable()
        }
    }
}
